package p1;

import java.util.Scanner;

public class DivProblem {
	
	static int numDiv(int a)
	{
		int counter=0;
		for(int i=1; i<=a/2; i++)
		{
			if(a%i==0)
			{
				counter+=2;
			}
			
		}
		counter+=2;
		
		return counter;
	}
	
	 static int compare(int a, int b)
	{
		int counter1, counter2;
			
		counter1=numDiv(a);
		counter2=numDiv(b);
		 
		
		if(counter1==counter2)
		{
			return 1;
		}
		else
		{
			return 2;
		}
		
		
	}
	
	public static void main(String[] arg)
	{
		int a, b;
		Scanner in = new Scanner(System.in); 
	    System.out.printf("Introduceti valoare pentru a: ");
	    a = in.nextInt();
	    System.out.printf("Introduceti valoare pentru b: ");
	    b = in.nextInt();
	    
	    if((a==0 && b!=0) || (b==0 && a!=0))
	    {
	    	if(a==0)
	    	{
	    		System.out.println("Numarul " + a +" are un numar infinit de diviori, iar numarul "+ b + " are un numar finit de divizori");
	    		
	    	}
	    	else
	    	{
	    		System.out.println("Numarul " + b +" are un numar infinit de diviori, iar numarul "+ a + " are un numar finit de divizori");
	    	}
	    	
	    }
	    else if(compare(a,b)==1)
	    {
	    	System .out.println("Numerele "+a+" si "+b+" au acelasi numar total de divizori.");
	    }
	    else
	    {
	    	System .out.println("Numerele "+a+" si "+b+" nu au acelasi numar total de divizori.");
	    }
	    
	    in.close();
	    
	}

}
